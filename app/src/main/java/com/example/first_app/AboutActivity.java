package com.example.first_app;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import Model.State;
import api.Utils;

public class AboutActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_text_view);
        State state = setupState();
        setupView(state);

    }

    State setupState(){
        State state = new State();
        state.name = getIntent().getStringExtra("name");
        state.capital = getIntent().getStringExtra("capital");
        state.alpha3Code = getIntent().getStringExtra("alpha");
        state.region = getIntent().getStringExtra("region");
        state.flag = getIntent().getStringExtra("flag");
        return state;
    }

    void setupView(State state){
        TextView txCapital = findViewById(R.id.capital);
        TextView txTitle = findViewById(R.id.detailTitle);
        //TextView txAlpha = findViewById(R.id.alpha);
        TextView txRegion = findViewById(R.id.region);
        ImageView imFlag = findViewById(R.id.image);

        //Log.e("flag", "setupView: " + state.flag );
        //Picasso.get().load(state.flag).into(imFlag);
        Utils.fetchSvg(this, state.flag, imFlag);

        txCapital.setText(state.capital);
        txTitle.setText(state.name);
        txTitle.setTextSize(25);
        txRegion.setText(state.region);
        //txAlpha.setText("Alpha Code: " + state.alpha3Code);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
