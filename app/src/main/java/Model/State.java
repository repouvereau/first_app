package Model;

public class State {
    public String name;
    public String capital;
    public String alpha3Code;
    public String alpha2Code;
    public String region;
    public String flag;

    public State(){
        this.name = "Unknown";
        this.capital = "Unknown";
        this.alpha3Code= "Unknown";
        this.region= "Unknown";
        this.flag="Unknown";
        this.alpha2Code="Unknown";
    }
}
